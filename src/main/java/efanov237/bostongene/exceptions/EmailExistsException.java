package efanov237.bostongene.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmailExistsException extends Exception {
    public EmailExistsException(String email) {
        super("User with email '" + email + "' is already exists!");
    }
}
