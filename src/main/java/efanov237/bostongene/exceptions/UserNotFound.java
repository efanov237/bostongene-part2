package efanov237.bostongene.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFound extends Exception {
    public UserNotFound(String param, String value) {
        super("User with " + param + "=" + value + " was not found!");
    }
}