package efanov237.bostongene.service.impl;

import efanov237.bostongene.domain.User;
import efanov237.bostongene.exceptions.EmailExistsException;
import efanov237.bostongene.exceptions.UserNotFound;
import efanov237.bostongene.repository.UserRepository;
import efanov237.bostongene.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class SpringDataUserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SpringDataUserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User add(User user) throws EmailExistsException {
        String email = user.getEmail();
        if (!repository.existsByEmail(email)) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            repository.save(user);
        } else {
            throw new EmailExistsException(email);
        }
        return user;
    }

    @Override
    public void deleteByEmail(String email) throws UserNotFound {
        if (repository.existsByEmail(email)) {
            repository.deleteByEmail(email);
        } else {
            throw new UserNotFound("email", email);
        }
    }

    @Transactional(readOnly = true)
    @Override
    public User findByEmail(String email) throws UserNotFound {
        User user = repository.findByEmail(email);
        if (user != null) {
            return repository.findByEmail(email);
        } else {
            throw new UserNotFound("email", email);
        }
    }
}
