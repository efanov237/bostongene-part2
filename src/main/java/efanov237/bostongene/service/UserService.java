package efanov237.bostongene.service;

import efanov237.bostongene.domain.User;
import efanov237.bostongene.exceptions.EmailExistsException;
import efanov237.bostongene.exceptions.UserNotFound;

public interface UserService {
    User add(User user) throws EmailExistsException;

    void deleteByEmail(String email) throws UserNotFound;

    User findByEmail(String email) throws UserNotFound;
}
