package efanov237.bostongene.controller;

import efanov237.bostongene.domain.User;
import efanov237.bostongene.exceptions.EmailExistsException;
import efanov237.bostongene.exceptions.UserNotFound;
import efanov237.bostongene.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "{email:.+}")
    public User findByEmail(@PathVariable String email) throws UserNotFound {
        return userService.findByEmail(email);
    }

    @PostMapping
    public User addUser(@Valid @RequestBody User user) throws EmailExistsException {
        return userService.add(user);
    }

    @DeleteMapping(value = {"{email:.+}"})
    public HttpStatus deleteByEmail(@PathVariable String email) throws UserNotFound {
        userService.deleteByEmail(email);
        return HttpStatus.OK;
    }
}
