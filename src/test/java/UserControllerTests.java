import efanov237.bostongene.Application;
import efanov237.bostongene.domain.User;
import efanov237.bostongene.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;



//ToDo: tests for xml
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserControllerTests {
    private final static String USERS_PATH = "/users/";
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mvc;


    @Autowired
    private HttpMessageConverter<Object> mappingJackson2HttpMessageConverter;

    private User existingUser;
    private User newUser;

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() {
        this.mvc = webAppContextSetup(webApplicationContext).build();

        this.userRepository.deleteAll();

        this.existingUser = new User("John", "Doe", LocalDate.now(),
                "test@test.com", encoder.encode("pass"));
        this.newUser = new User("John", "Smith", LocalDate.now(),
                "js@test.com", "password");
        this.userRepository.save(existingUser);
    }


    @Test
    public void findUserByEmailJson() throws Exception {
        mvc.perform(get(USERS_PATH + "/" + existingUser.getEmail()).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("firstName", is(existingUser.getFirstName())))
                .andExpect(jsonPath("email", is(existingUser.getEmail())));
    }

    @Test
    public void findUserByEmailNotFound() throws Exception {
        mvc.perform(get(USERS_PATH + "/not@existing.address"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addUser() throws Exception {
        String json = json(newUser);
        mvc.perform(post(USERS_PATH).contentType(contentType).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", not(empty())));
    }

    @Test
    public void addUserIfEmailExists() throws Exception {
        String json = json(existingUser);
        mvc.perform(post(USERS_PATH).contentType(contentType).content(json));
    }

    @Test
    public void deleteUserByEmail() throws Exception {
        mvc.perform(delete("/users/test@test.com/"));
    }

    @Test
    public void deleteUserByEmailNotFound() throws Exception {
        mvc.perform(delete(USERS_PATH + "/not@exitisting.address"))
                .andExpect(status().isNotFound());
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
